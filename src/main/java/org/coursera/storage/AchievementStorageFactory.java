package org.coursera.storage;

public final class AchievementStorageFactory {

    private static AchievementStorage achievementStorage;

    public static AchievementStorage getAchievementStorage() {
        return AchievementStorageFactory.achievementStorage;
    }

    public static void setAchievementStorage(AchievementStorage achievementStorage) {
        AchievementStorageFactory.achievementStorage = achievementStorage;
    }
}
