/**
 * @file ForumService.java
 * @brief Interface para proxy de gamificação
 *
 * @author Uilian Ries <uilianries@gmail.com>
 * @license MIT License, Copyright (c) 2017 Uilian Ries
 */
package org.coursera.gamification;

public interface ForumService {

    /**
     * Deve adicionar 5 pontos do tipo "CREATION". Deve adicionar o bagde "I CAN TALK"
     * @param user
     * @param topic
     */
    void addTopic(String user, String topic);

    /**
     * Deve adicionar 3 pontos do tipo "PARTICIPATION". Deve adicionar o badge "LET ME ADD"
     * @param user
     * @param topic
     * @param comment
     */
    void addComment(String user, String topic, String comment);

    /**
     * Deve adicionar 1 ponto do tipo "CREATION".
     * @param user
     * @param topic
     * @param topicUser
     */
    void likeTopic(String user, String topic, String topicUser);

    /**
     * Deve adicionar 1 ponto do tipo "PARTICIPATION".
     * @param user
     * @param topic
     * @param comment
     * @param commentUser
     */
    void likeComment(String user, String topic, String comment, String commentUser);
}
