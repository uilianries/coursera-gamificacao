package org.coursera.achievement;

import org.junit.Test;

import static org.junit.Assert.*;

public class PointsTest {

    @Test
    public void getScore() throws Exception {
        Points points = new Points("Foo", 42);
        assertEquals(new Integer(42), points.getScore());
        assertEquals("Foo", points.getName());
    }

    @Test
    public void add() throws Exception {
        Points pointsFirst = new Points("Foo", 42);
        Points pointsSecond = new Points("Foo", 24);
        pointsFirst.add(pointsSecond);

        assertEquals(new Integer(66), pointsFirst.getScore());
        assertEquals("Foo", pointsFirst.getName());
    }

    @Test
    public void BadNameTest() {
        try {
            Points pointsFirst = new Points("Foo", 42);
            Points pointsSecond = new Points("Bar", 24);
            pointsFirst.add(pointsSecond);
            fail();
        } catch (final RuntimeException e) {
            assertEquals("Invalid name.", e.getMessage());
        }
    }

    @Test
    public void BadTypeTest() {
        try {
            Points pointsFirst = new Points("Foo", 42);
            Badge pointsSecond = new Badge("Foo");
            pointsFirst.add(pointsSecond);
            fail();
        } catch (final RuntimeException e) {
            assertEquals("Invalid Achievement type.", e.getMessage());
        }
    }

}