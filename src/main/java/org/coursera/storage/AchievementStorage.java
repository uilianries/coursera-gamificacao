package org.coursera.storage;

import org.coursera.achievement.Achievement;

import java.util.List;


public interface AchievementStorage {

    public void addAchievement(final String user, final Achievement achievement);

    public Achievement getAchievement(final String user, final String achievementName);

    public List<Achievement> getAchievements(final String user);
}
