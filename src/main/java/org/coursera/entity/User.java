package org.coursera.entity;

import org.coursera.achievement.Achievement;
import org.coursera.achievement.Badge;
import org.coursera.achievement.Points;
import org.coursera.notification.AchievementObserver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class User implements AchievementObserver {

    public String name;
    public HashSet<Badge> badges = new HashSet<>();
    public HashSet<Points> points = new HashSet<>();

    public User(final String name) {
        this.name = name;
    }

    public void addBadge(final Badge badge) {
        badges.add(badge);
    }

    public void addPoints(final Points point) {
        if (this.points.contains(point)) {
            for (Points it : this.points) {
                if (it.equals(point)) {
                    it.add(point);
                    this.points.add(it);
                    break;
                }
            }
        } else {
            this.points.add(point);
        }
    }

    public Achievement getAchievement(final String achievementName) {
        for (Points it : this.points) {
            if (it.getName().equals(achievementName))
                return it;
        }
        for (Badge it : this.badges) {
            if (it.getName().equals(achievementName))
                return  it;
        }
        throw new RuntimeException("Achievement not found.");
    }

    public List<Achievement> getAchievements() {
        ArrayList<Achievement> achievements = new ArrayList<>();
        achievements.addAll(this.points);
        achievements.addAll(this.badges);
        return achievements;
    }


        @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof User))
            return false;
        User other = (User) obj;
        return this.name.equals(other.name);
    }

    @Override
    public void achievementUpdate(String user, Achievement achievement) {
        if (Objects.equals(user, this.name)) {
            if (achievement instanceof Points) {
                Points points = (Points) achievement;
                if (points.getName().equals("CREATION")) {
                    for (Points it : this.points) {
                        if (it.equals(points) && it.getScore() >= 100) {
                            addBadge(new Badge("INVENTOR"));
                            break;
                        }
                    }
                } else if (points.getName().equals("PARTICIPATION")) {
                    for (Points it : this.points) {
                        if (it.equals(points) && it.getScore() >= 100) {
                            addBadge(new Badge("PART OF THE COMMUNITY"));
                            break;
                        }
                    }
                }
            }
        }
    }
}
