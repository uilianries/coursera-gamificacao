/*!
 * @file AchievementStorageTest.java
 * @brief Valida o armazenamento das informações
 *
 * @author Uilian Ries <uilianries@mmail.com>
 * @license MIT License, Copyright (c) 2017 Uilian Ries
 */
package org.coursera.storage;

import org.coursera.achievement.Achievement;
import org.coursera.achievement.Points;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AchievementStorageTest {
    private AchievementStorage achievementStorage;

    @org.junit.BeforeClass
    public static void setupClass() {
        AchievementStorageFactory.setAchievementStorage(MemoryAchievementStorage.INSTANCE);
    }

    @org.junit.Before
    public void setUp() {
        achievementStorage = AchievementStorageFactory.getAchievementStorage();
    }


    @Test
    public void TestAddAchievement() {
        final String user = "Foobar";
        final String achievementName = "Couse";

        achievementStorage.addAchievement(user, new Points(achievementName, 10));
        final Achievement result = achievementStorage.getAchievement(user, achievementName);
        assertEquals(achievementName, result.getName());
    }
}
