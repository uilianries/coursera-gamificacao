/**
 * @file Achievement.java
 * @brief Representa uma conquista
 *
 * @author Uilian Ries <uilianries@gmail.com>
 * @license MIT License, Copyright (c) 2017 Uilian Ries
 */
package org.coursera.achievement;

public abstract class Achievement {
    /** Identifica o Achievement */
    private String name;

    public Achievement(final String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public abstract void add(Achievement achievement);

    @Override
    public int hashCode() {
        return this.getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Achievement))
            return false;
        Achievement other = (Achievement) obj;
        return this.name.equals(other.name);
    }
}
