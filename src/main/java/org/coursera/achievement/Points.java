/**
 * @file Points.java
 * @brief Representa um conquista com pontuação
 *
 * @author Uilian Ries <uilianries@gmail.com>
 * @license MIT License, Copyright (c) 2017 Uilian Ries
 */
package org.coursera.achievement;


public class Points extends Achievement {
    /** Pontuação dada a conquista */
    Integer score;

    /**
     * Inicia uma nova pontução
     * @param name Nome da conquista
     * @param score Valor da pontução inicial
     */
    public Points(String name, final Integer score) {
        super(name);
        this.score = score;
    }

    /**
     * Recupera pontução
     * @return Pontuação atual
     */
    public Integer getScore() {
        return this.score;
    }

    /**
     * Incrementa pontução atual
     * @param achievement Nova pontuação
     */
    @Override
    public void add(final Achievement achievement) {
        if (!this.getName().equals(achievement.getName())) {
            throw new RuntimeException("Invalid name.");
        }

        if (!(achievement instanceof Points)) {
            throw new RuntimeException("Invalid Achievement type.");
        }

        final Points points = (Points) achievement;
        this.score += points.score;
    }
}
