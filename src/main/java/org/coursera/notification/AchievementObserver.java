/**
 * @file AchievementObserver.java
 * @brief Interface para observar eventos de gamificação
 *
 * @author Uilian Ries <uilianries@gmail.com>
 * @license MIT License, Copyright (c) 2017 Uilian Ries
 */
package org.coursera.notification;

import org.coursera.achievement.Achievement;

public interface AchievementObserver {

    /**
     * Recebe atualização sobre usuário e conquista
     * @param user Nome to usuário alvo
     * @param achievement Nome da conquista interessada
     */
    public void achievementUpdate(String user, Achievement achievement);
}
