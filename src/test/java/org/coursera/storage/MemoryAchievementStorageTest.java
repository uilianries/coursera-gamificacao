package org.coursera.storage;

import org.coursera.achievement.Achievement;
import org.coursera.achievement.Badge;
import org.coursera.achievement.Points;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class MemoryAchievementStorageTest {
    @Test
    public void addAchievement() throws Exception {
        MemoryAchievementStorage storage = MemoryAchievementStorage.INSTANCE;
        storage.addAchievement("Blah", new Points("Bar", 100));
        storage.addAchievement("Blah", new Badge("Qux"));

        final List<Achievement> achievements = storage.getAchievements("Blah");
        assertEquals(2, achievements.size());
        assertTrue(achievements.contains(new Points("Bar", 100)));
        assertTrue(achievements.contains(new Badge("Qux")));
    }

    @Test
    public void getAchievement() throws Exception {
        MemoryAchievementStorage storage = MemoryAchievementStorage.INSTANCE;
        storage.addAchievement("FooBar", new Points("Bar", 100));
        storage.addAchievement("FooBar", new Badge("Qux"));

        final Achievement achievement = storage.getAchievement("FooBar", "Bar");
        assertEquals(new Points("Bar", 100), achievement);
    }

    @Test
    public void BadName() {
        MemoryAchievementStorage storage = MemoryAchievementStorage.INSTANCE;
        storage.addAchievement("Gorse", new Points("Bar", 100));
        storage.addAchievement("Gorse", new Badge("Qux"));

        try {
            storage.getAchievements("GorseFoo");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Name not found.", e.getMessage());
        }
    }

    @Test
    public void BadAchievement() {
        MemoryAchievementStorage storage = MemoryAchievementStorage.INSTANCE;
        storage.addAchievement("Blahz", new Points("Bar", 100));
        storage.addAchievement("Blahz", new Badge("Qux"));

        try {
            storage.getAchievement("Blahz", "Couse");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Achievement not found.", e.getMessage());
        }
    }

}