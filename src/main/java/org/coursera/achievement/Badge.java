/**
 * @file Badge.java
 * @brief Representa um objetivo conquistado
 *
 * @author Uilian Ries <uilianries@gmail.com>
 * @license MIT License, Copyright (c) 2017 Uilian Ries
 */
package org.coursera.achievement;

public class Badge extends Achievement {

    /**
     * Inicia objetivo
     * @param name Nome do objetivo
     */
    public Badge(String name) {
        super(name);
    }

    @Override
    public void add(Achievement achievement) {
        // Do nothing
    }
}
