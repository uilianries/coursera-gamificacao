/**
 * @file ForumServiceMock.java
 * @brief Mock encapsulado pelo proxy de gamificação
 *
 * @author Uilian Ries <uilianries@gmail.com>
 * @license MIT License, Copyright (c) 2017 Uilian Ries
 */
package org.coursera.gamification;


import org.coursera.achievement.Badge;
import org.coursera.achievement.Points;
import org.coursera.storage.AchievementStorage;
import org.coursera.storage.AchievementStorageFactory;

public class ForumServiceMock implements ForumService {

    private AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();

    @Override
    public void addTopic(String user, String topic) {
        storage.addAchievement(user, new Points("CREATION", 5));
        storage.addAchievement(user, new Badge("I CAN TALK"));
    }

    @Override
    public void addComment(String user, String topic, String comment) {
        storage.addAchievement(user, new Points("PARTICIPATION", 3));
        storage.addAchievement(user, new Badge("LET ME ADD"));
    }

    @Override
    public void likeTopic(String user, String topic, String topicUser) {
        storage.addAchievement(user, new Points("CREATION", 1));
    }

    @Override
    public void likeComment(String user, String topic, String comment, String commentUser) {
        storage.addAchievement(user, new Points("PARTICIPATION", 1));
    }
}
