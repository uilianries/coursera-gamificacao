/*!
 * @file AchievementStorageTest.java
 * @brief Valida o armazenamento das informações
 *
 * @author Uilian Ries <uilianries@mmail.com>
 * @license MIT License, Copyright (c) 2017 Uilian Ries
 */
package org.coursera.gamification;

import org.coursera.achievement.Achievement;
import org.coursera.achievement.Badge;
import org.coursera.achievement.Points;
import org.coursera.storage.AchievementStorage;
import org.coursera.storage.AchievementStorageFactory;
import org.coursera.storage.MemoryAchievementStorage;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GamificationTest {

    @Before
    public void setUp() {
        AchievementStorageFactory.setAchievementStorage(MemoryAchievementStorage.INSTANCE);
    }

    @Test
    public void TestAddTopic() {
        ForumService forumService = new ForumServiceGamificationProxy(new ForumServiceMock());
        forumService.addTopic("Foo", "Bar");

        AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();

        final Points points = (Points) storage.getAchievement("Foo", "CREATION");
        assertEquals(new Integer(5), points.getScore());
        assertEquals("CREATION", points.getName());

        final Badge badge = (Badge) storage.getAchievement("Foo", "I CAN TALK");
        assertEquals("I CAN TALK", badge.getName());

        assertEquals(2, storage.getAchievements("Foo").size());
    }

    @Test
    public void TestAddCoupleTopics() {
        ForumService forumService = new ForumServiceGamificationProxy(new ForumServiceMock());
        forumService.addTopic("quuz", "Bar");
        forumService.addTopic("quuz", "Foo");

        AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();

        final Points points = (Points) storage.getAchievement("quuz", "CREATION");
        assertEquals(new Integer(10), points.getScore());
        assertEquals("CREATION", points.getName());

        final Badge badge = (Badge) storage.getAchievement("quuz", "I CAN TALK");
        assertEquals("I CAN TALK", badge.getName());

        assertEquals(2, storage.getAchievements("quuz").size());
    }

    @Test
    public void TestAddComment() {
        ForumService forumService = new ForumServiceGamificationProxy(new ForumServiceMock());
        forumService.addComment("Bar", "Bar", "Qux");

        AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();

        final Points points = (Points) storage.getAchievement("Bar", "PARTICIPATION");
        assertEquals(new Integer(3), points.getScore());
        assertEquals("PARTICIPATION", points.getName());

        final Badge badge = (Badge) storage.getAchievement("Bar", "LET ME ADD");
        assertEquals("LET ME ADD", badge.getName());

        assertEquals(2, storage.getAchievements("Bar").size());
    }

    @Test
    public void TestLikeTopic() {
        ForumService forumService = new ForumServiceGamificationProxy(new ForumServiceMock());
        forumService.likeTopic("Baz", "Bar", "Qux");

        AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();

        final Points points = (Points) storage.getAchievement("Baz", "CREATION");
        assertEquals(new Integer(1), points.getScore());
        assertEquals("CREATION", points.getName());

        assertEquals(1, storage.getAchievements("Baz").size());
    }

    @Test
    public void TestLikeComment() {
        ForumService forumService = new ForumServiceGamificationProxy(new ForumServiceMock());
        forumService.likeComment("Qux", "Bar", "Qux", "Baz");

        AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();

        final Points points = (Points) storage.getAchievement("Qux", "PARTICIPATION");
        assertEquals(new Integer(1), points.getScore());
        assertEquals("PARTICIPATION", points.getName());

        assertEquals(1, storage.getAchievements("Qux").size());
    }

    @Test
    public void TestAchievementInventor() {
        ForumService forumService = new ForumServiceGamificationProxy(new ForumServiceMock());
        for (int i = 0; i < 20; i++)
            forumService.addTopic("Fred", "Bar");

        AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();

        final Points points = (Points) storage.getAchievement("Fred", "CREATION");
        assertEquals(new Integer(100), points.getScore());
        assertEquals("CREATION", points.getName());

        Badge badge = (Badge) storage.getAchievement("Fred", "I CAN TALK");
        assertEquals("I CAN TALK", badge.getName());

        badge = (Badge) storage.getAchievement("Fred", "INVENTOR");
        assertEquals("INVENTOR", badge.getName());

        assertEquals(3, storage.getAchievements("Fred").size());
    }

    @Test
    public void TestAchievementCommunity() {
        ForumService forumService = new ForumServiceGamificationProxy(new ForumServiceMock());
        for (int i = 0; i < 34; i++)
            forumService.addComment("Waldo", "Bar", "Foo");

        AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();

        final Points points = (Points) storage.getAchievement("Waldo", "PARTICIPATION");
        assertEquals(new Integer(102), points.getScore());
        assertEquals("PARTICIPATION", points.getName());

        Badge badge = (Badge) storage.getAchievement("Waldo", "LET ME ADD");
        assertEquals("LET ME ADD", badge.getName());

        badge = (Badge) storage.getAchievement("Waldo", "PART OF THE COMMUNITY");
        assertEquals("PART OF THE COMMUNITY", badge.getName());

        assertEquals(3, storage.getAchievements("Waldo").size());
    }

}
