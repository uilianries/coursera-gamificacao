package org.coursera.storage;

import org.coursera.achievement.Achievement;
import org.coursera.achievement.Badge;
import org.coursera.achievement.Points;
import org.coursera.entity.User;

import java.util.HashSet;
import java.util.List;


public enum MemoryAchievementStorage implements AchievementStorage {

    INSTANCE;

    private HashSet<User> users = new HashSet<User>();

    private MemoryAchievementStorage() {}

    @Override
    public void addAchievement(String user, Achievement achievement) {
        if (!users.contains(user)) {
            users.add(new User(user));
        }
        for (User it : users) {
            if (it.name.equals(user)) {
                if (achievement instanceof Points)
                    it.addPoints((Points)achievement);
                else
                    it.addBadge((Badge) achievement);
                break;
            }
        }
        for (User it : users) {
            it.achievementUpdate(user, achievement);
        }
    }

    @Override
    public Achievement getAchievement(String user, String achievementName) {
        for (User it : users) {
            if (it.name.equals(user)) {
                return it.getAchievement(achievementName);
            }
        }
        throw new RuntimeException("Name not found.");
    }

    @Override
    public List<Achievement> getAchievements(final String user) {
        for (User it : users) {
            if (it.name.equals(user)) {
                return it.getAchievements();
            }
        }
        throw new RuntimeException("Name not found.");
    }

}
